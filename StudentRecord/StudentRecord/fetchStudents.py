import json
import csv
from students.models import Student, Campus, Program

def ToJson(programPath, profilePath, jsonPath):

    data = {}

    with open(profilePath, encoding='utf-8') as csvf:   #reader1 stores student profile info
        reader1 = csv.DictReader(csvf)
        
                
        for row in reader1:     #runs for each student to store the student profile
            key1 = row['student_username']
            student = {}        #student dict contains all the student info
            student["campus"] = row['campus_identifier']
            student["gpa"] = float(row['gpa'])
            student["email"] = row['email']
            student["first_name"] = row['first_name']
            student["last_name"] = row['last_name']
            student["username"] = row['student_username']

            record = {}         #record dict contains student and program info both

            with open(programPath, encoding='utf-8') as csvf2:  #reader2 stores student program info
                reader2 = csv.DictReader(csvf2)
                
                plan = {}
                programs = {}
                programList = []            #stores all programs for a student
                for row2 in reader2:        #checks all records in program info file
                    key2 = row2['student_username']
                    if key2 == key1:        
                        programList.append(row2['program_identifier'])

                programs['programs'] = programList
                plan['plan'] = programs

                record['plan'] = programs
                record['student'] = student
                
                data[key1] = record
                

    with open(jsonPath, 'w', encoding='utf-8') as jsonf:
        jsonf.write(json.dumps(data, indent=4))

def ParseJson(path):
    file = open(path)
    data = json.load(file)
    student = []        #list of student data
    for s in data:      #runs for each student
        programList = data[s]["plan"]["programs"]       #list of programs
        studentInfo = data[s]["student"]                #dict of student info
        student.append([s, studentInfo, programList])   #list of 3 (username, dict studentInfo, programList)

    return student 

def PopulateDatabase(students):
    for record in students:
        user = record[0]
        if len(record[2]) == 2:
            program1 = record[2][0]
            program2 = record[2][1]

            p, createdp = Program.objects.get_or_create(name = program1)
            p2, createdp2 = Program.objects.get_or_create(name = program2)
            
        else:
            program1 = record[2][0]
            
            p, createdp = Program.objects.get_or_create(name = program1)
            
            p2 = None
            
        first = record[1]["first_name"]
        second = record[1]["last_name"]
        gp = record[1]["gpa"]
        mail = record[1]["email"]
        campus = record[1]["campus"]

    

        c, created = Campus.objects.get_or_create(name = campus)
        s, created = Student.objects.get_or_create(username = user, firstname = first, lastname = second, email = mail, campus = c, program1 = p, program2 = p2, gpa = gp)

    print("\nThese objects added in Campus table:")    
    print(Campus.objects.all())
    print("\nThese objects added in Program table:")   
    print(Program.objects.all())
    print("\nThese objects added in Student table:")   
    print(Student.objects.all())
        
        
        

        
def main():
    profilePath = r'student_profile.csv'
    jsonPath = r'student.json'
    programPath = r'student_program.csv'
    ToJson(programPath, profilePath, jsonPath)
    students =  ParseJson(jsonPath)
    PopulateDatabase(students)
        
s = main()

