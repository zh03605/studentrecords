# Generated by Django 3.2.8 on 2021-10-28 09:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Campus',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cname', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Program',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pname', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=20, verbose_name='Username')),
                ('firstname', models.CharField(max_length=20, verbose_name='First Name')),
                ('lastname', models.CharField(max_length=20, verbose_name='Last Name')),
                ('email', models.CharField(max_length=40, verbose_name='Email')),
                ('gpa', models.IntegerField(verbose_name='GPA')),
                ('Campus', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='students.campus')),
                ('Program1', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='primary_program', to='students.program')),
                ('Program2', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='students.program')),
            ],
        ),
    ]
