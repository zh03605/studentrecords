from django.db import models
from django.contrib.auth.models import User



class Program(models.Model):
    name = models.CharField(max_length=20, default = "")
    def __str__(self):
        return self.name

class Campus(models.Model):
    name = models.CharField(max_length=20)
    def __str__(self):
        return self.name

class Student(models.Model):
    username = models.CharField('Username', max_length=20)
    firstname = models.CharField('First Name', max_length=20)
    lastname = models.CharField('Last Name', max_length=20)
    email = models.CharField('Email', max_length=40)
    gpa = models.FloatField('GPA')
    campus = models.ForeignKey(Campus, on_delete=models.CASCADE)
    program1 = models.ForeignKey(Program, on_delete=models.CASCADE, related_name='primary_program')
    program2 = models.ForeignKey(Program,on_delete=models.CASCADE, null = True)

    def __str__(self):
        if self.program2 != None:
            return self.username + " " + str(self.gpa) + " " + self.campus.name + " " + self.program1.name + " " + self.program2.name
        return self.username + " " + str(self.gpa) + " " + self.campus.name + " " + self.program1.name 
