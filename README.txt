This guide assumes Python and Django packages are installed on the machine. 

Download/Clone the repository on your local machine and follow the steps given below:
1. Open Command Prompt.
2. Go to the directory where manage.py is located and run the following commands:
3. py manage.py shell
4. exec(open("fetchStudents.py").read())

What does this Django app do?
It reads the CSV files, student_profile.csv and student_program.csv, and saves the student information in a JSON file, student.json. 
Next, it deserializes the student.json file and stores the data it in a SQLite Database using the Django library in Python. 
The database contains 3 tables (or models), Student, Program and Campus. Each student can belong to 1 campus and 2 programs, program1 and program2. 
We have resolved the many-to-many relationship between Student and Program into a one-to-many relationship.The assummtion we have made here is that a Student can have only 1 or 2 Programs.

Note: Make sure your JSON and CSV files are in the same location as FetchStudents.py and manage.py files. 